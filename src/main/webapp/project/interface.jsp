<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<!DOCTYPE html>
<%@ include file="../common/comm_css.jsp"%>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>接口列表</title>
		<meta name="keywords" content="">
		<meta name="description" content="">

		<style type="text/css">
			.h_top {
				width: 100%;
				position: fixed;
				top: 0px;
				background: #fff;
				padding-top: 15px;
				padding-bottom: 10px;
				z-index: 100;
				box-shadow: 0px 3px 3px #A7AAAB;
			}
			
			.h_right {
				position: fixed;
				right: 21px;
				top: 50%;
				background: #fff;
				z-index: 9999;
				padding: 10px 15px;
				border: 1px solid #eee;
			}
			.hr-line-dashed{
				margin: 10px 0;
			}
			
			.vertical-line{
				border-left: 1px dashed #e7eaec;
			    margin: 0 10px;
			}
		</style>
	</head>

	<body class="gray-bg">
		<div class="h_top form-inline ibox-content">
			<div class="col-lg-6 col-xs-8 col-md-7">
				<div class="dataTables_length"><label>接口名 <input id="interface_name" style="width:110px;" type="search" class="form-control input-sm" aria-controls="DataTables_Table_0"></label></div>
				<div class="dataTables_length"><label style="padding-left:15px;">URL <input id="interface_url" style="width:110px;" type="search" class="form-control input-sm" aria-controls="DataTables_Table_0"></label></div>
				<div class="dataTables_length"><label style="padding-left:15px;">编号 <input id="interface_num" style="width:110px;" type="search" class="form-control input-sm" aria-controls="DataTables_Table_0"></label></div>
			</div>
			<div class="col-lg-3 col-xs-4 col-md-3" style="text-align: right; float:right;">
				<a class="btn mar-t-15 ml15 fl btn-small btn-primary btn-sub" onclick="getInterfaceList();">搜索</a>
				<a class="btn mar-t-15 ml15 fl btn-small btn-danger btn-sub" onclick="clearVal();">清除</a>
				<a class="btn mar-t-15 ml15 fl btn-small btn-info btn-sub" onclick="location.href='add_inter.jsp?pro_id='+getURLparam('pro_id')+'&&module_id='+getURLparam('module_id')+''">新增</a>
			</div>
		</div>

		<div class="h_right">
			<!-- <a style="color: #aaa;">导出<br />预览</a>
			<hr style="margin-top: 10px;margin-bottom: 10px;" /> -->
			<a id="backtotop" style="color: #aaa;">返回<br/>顶部</a>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight" style="margin-top: 60px;">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>接口列表</h5>
						</div>
						<div class="ibox-content">
						  	<pre><code id="in_explain"></code></pre>
							<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline" role="grid">
								<table class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th style="text-align: center;">选择</th>
											<th style="text-align: center;">SN</th>
											<th style="text-align: center;">编号</th>
											<th>接口名称</th>
											<th>URL</th>
											<th>描述</th>
											<th style="text-align: center;">图片</th>
											<th style="text-align: center;">作者</th>
											<th style="text-align: center;">更新时间</th>
											<th style="text-align: center;">标注</th>
											<th style="text-align: center;">操作</th>
										</tr>
									</thead>
									<tbody id="listgroup">
										<!-- 
										<tr>
											<td style="text-align: center;">
												<div class="checkbox i-checks">
													<label><input type="checkbox" value=""></label>
												</div>
											</td>
											<td style="text-align: center;">@{num}</td>
											<td style="text-align: center;">#{list[i].interface_num}</td>
											<td>#{list[i].interface_name}</td>
											<td>#{list[i].interface_url}</td>
											<td>@{remark}</td>
											<td style="text-align: center;">@{getHas}</td>
											<td style="text-align: center;">#{list[i].operator}</td>
											<td style="text-align: center;">@{getTime}</td>
											<td style="text-align: center;">@{getState}</td>
											<td style="text-align: center;">@{getOpt}</td>
										</tr>
										<tr id="tr@{num}" hidden="hidden">
											<td colspan="11">
												<div class="row">
													<div class="col-lg-12">
														<table id="table@{num}" class="table table-bordered dataTables-example" style="margin-bottom: 0px;">
															<tbody>
																<tr>
																	<td class="col-lg-1 text-right"><b>编号</b></td>
																	<td class="col-lg-1">#{list[i].interface_num}</td>
																	<td class="col-lg-1 text-right"><b>接口名称</b></td>
																	<td colspan="3">#{list[i].interface_name}</td>
																	<td rowspan="6" class="col-lg-4" style="text-align: center;">
																		<a id="img_url@{num}" class="fancybox" href="@{imgUrl}">
																			<img id="img@{num}" alt="image" style="width: auto;height: auto; max-width: 100%;vertical-align: middle;"/>
																		</a>
																	</td>
																</tr>
																<tr>
																	<td colspan="6">#{list[i].interface_remark}</td>
																</tr>
																<tr>
																	<td class="col-lg-1 text-right"><b>请求方式</b></td>
																	<td class="col-lg-1">@{interfaceType}</td>
																	<td class="col-lg-1 text-right"><b>URL</b></td>
																	<td colspan="3">#{list[i].interface_url}</td>
																</tr>
																<tr>
																	<td class="text-right"><b>请求参数</b></td>
																	<td colspan="5" style="padding: 2px 2px;">
																		<table class="table table-bordered dataTables-example" style="margin-bottom: 0px;">
																			<tbody>
																				@{getParam}
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td class="text-right"><b>返回对象</b></td>
																	<td colspan="4" style="padding: 2px 2px;">
																		<pre style="margin:0 0;"><code>@{return_text}</code></pre>
																	</td>
																</tr>
																<tr>
																	<td class="text-right"><b>响应码</b></td>
																	<td colspan="4">
																		@{return_code}
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</td>
										</tr>
										 -->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="listgroup2">
			<!-- 
				<tr>
					<td class="col-lg-2">#{array[x].c_name}</td>
					<td class="col-lg-1">#{array[x].c_type}</td>
					@{_check}
					<td>#{array[x].c_remark}</td>
				</tr>
			 -->
		</div>
		
	</body>

</html>
<script src="${pageContext.request.contextPath}/js/content.js?v=1.0.0"></script>
<script type="text/javascript">
XCOTemplate.pretreatment('listgroup');	
XCOTemplate.pretreatment('listgroup2');	

	$(document).ready(function() {
		//图片
		$('.fancybox').fancybox({
			openEffect: 'none',
			closeEffect: 'none'
		});
	});
	var x = 0;
	
	function getDetail(_id,_img_url){
		if(x==_id){
			$("#tr"+_id).toggle();
			x=0;
			return;
		}
		$("#tr"+_id).toggle();
		var height = $("#table"+_id).height()-21;
		//$("#img_url"+_id).css("max-height",height+"px");
		$("#img"+_id).css("max-height",height+"px");
		$("#img"+_id).attr("src",_img_url);
		if(x!=0){
			$("#tr"+x).toggle();
		}
		x=_id;
	}
	
	function clearVal(){
		$("#interface_num").val("");
		$("#interface_name").val("");
		$("#interface_url").val("");
	}

	getOnePro();
	function getOnePro(){
		var xco = new XCO();
		
		xco.setLongValue("id",getURLparam("pro_id"));
		var options = {
			url : "/manager/getOnePro.xco",
			data : xco,
			success : getOneProCallBack
		};
		$.doXcoRequest(options);
	}
	
	function getOneProCallBack(data){
		if(data.getCode()!=0){
			layer.msg(data.getMessage(), {time: times, icon:no});
		}else{
			$("#in_explain").text(data.getStringValue("in_explain"));
		}
	}
	
	getInterfaceList();
	function getInterfaceList() {
		var xco = new XCO();
		xco.setLongValue("pro_id", getURLparam("pro_id"));
		xco.setLongValue("mod_id", getURLparam("module_id"));

		var interface_num = $("#interface_num").val();
		if(interface_num){
			xco.setStringValue("interface_num",interface_num);
		}
		
		var interface_name = $("#interface_name").val();
		if(interface_name){
			xco.setStringValue("interface_name",interface_name);
		}
		
		var interface_url = $("#interface_url").val();
		if(interface_url){
			xco.setStringValue("interface_url",interface_url);
		}

		var options = {
			url : "/manager/getInterfaceList.xco",
			data : xco,
			success : getInterfaceListCallBack
		};
		$.doXcoRequest(options);
	}

	function getInterfaceListCallBack(data) {
		if (data.getCode() != 0) {
			layer.msg(data.getMessage(), {time: times, icon:no});
		} else {
			//console.log(data.toString());
			var total = 0;
			total = data.getIntegerValue("total");
			num = total;
			var len = 0;
			var list = data.getXCOListValue("list");
			
			var html = "";
			if (list) {
				len = list.length;
			}
			var extendedFunction = {
				getHas : function(){
					var img_url = list[i].getStringValue("img_url");
					if(img_url){
						return "有";
					}else{
						return "无";
					}
				},
				getTime : function(){
					return list[i].getStringValue("create_time");
				},
				getState : function(){
					var state = list[i].getIntegerValue("state");
					if(state==0){
						return '停用';
					}
					if(state==1){
						return '正常';
					}
				},
				getOpt : function(){
					var img_url = list[i].getStringValue("img_url");
					var num = i+1;
					if(img_url){
						return '<a onclick="getDetail('+num+',\''+img_url+'\')">查看</a>&nbsp;&nbsp;&nbsp;<a href="../project/edit_inter.jsp?pro_id='+getURLparam('pro_id')+'&&module_id='+getURLparam('module_id')+'&&interface_id='+list[i].getLongValue("interface_id")+'">编辑</a>';
					}else{
						return '<a onclick="getDetail('+num+',\'/img/wu.png\')">查看</a>&nbsp;&nbsp;&nbsp;<a href="../project/edit_inter.jsp?pro_id='+getURLparam('pro_id')+'&&module_id='+getURLparam('module_id')+'&&interface_id='+list[i].getLongValue("interface_id")+'">编辑</a>';
					}
					
				},
				imgUrl : function(){
					var img_url = list[i].getStringValue("img_url");
					if(img_url){
						return img_url;
					}else{
						return "/img/wu.png";
					}
				},
				interfaceType : function(){
					var interface_type = list[i].getIntegerValue("interface_type");
					if(interface_type==1){
						return 'Get';
					}
					if(interface_type==2){
						return 'Post';
					}
					if(interface_type==3){
						return 'Put';
					}
					if(interface_type==4){
						return 'Delete';
					}
				},
				getParam : function(){
					var param = list[i].getStringValue("interface_param");
					var xco1 = new XCO();
					xco1.fromXML(param);
					//alert(xco);
					var array = xco1.getXCOListValue("array");
					var _html="";
					var extendedFunction2 = {
						_check : function(xco){
							if(xco.get("array[x]").getIntegerValue("c_check")==1){
								return '	<td class="col-lg-1">必填</td>';
							}
							return '	<td class="col-lg-1">非必填</td>';
						}	
					}
					for ( var x = 0; x < array.length; x++) {
						xco1.setIntegerValue("x", x);
						_html += XCOTemplate.execute('listgroup2', xco1, extendedFunction2);
					}
					return _html;
				},
				num : function(){
					var num1 = i+1;
					return num1;
				},
				remark : function(){
					var interface_remark = list[i].getStringValue("interface_remark");
					if(interface_remark.length>15){
						return interface_remark.substring(0,15)+"...";
					}else{
						return interface_remark;
					}
				},
				return_text : function(){
					return __xcoUtil.encodeTextForXML(list[i].getStringValue("return_text"));
				},
				return_code : function(){
					var return_code = list[i].getStringValue("return_code").split("\n");
					var return_code_html = '';
					for(var j = 0;j<return_code.length;j++){
						return_code_html+=return_code[j]+"<br>";
					}
					return return_code_html;
				}
			};
			for ( var i = 0; i < len; i++) {
				data.setIntegerValue("i", i);
				html += XCOTemplate.execute('listgroup', data, extendedFunction);
			}
			document.getElementById('listgroup').innerHTML = html;
		}
	}
	
	
	$('#backtotop').click(function(){
   		// $('#backtotop').attr("href","#");
   		var scrtop = $(window).scrollTop();
   		var time,wct;
   		var x_scrtop = parseInt(scrtop/100);
   		//alert(scrtop+','+x_scrtop);
   		//$(window).scrollTop(0);
   			if(scrtop<=100){
   				$(window).scrollTop(0);
   			}else if(scrtop<=1000){
   				time = setInterval(function(){
   				scrtop = scrtop-100;
   				$(window).scrollTop(scrtop);
   				//alert(scrtop);
   				if(scrtop <= 0){
   					clearInterval(time);   			
   				}
   				},10);
   			}else if(scrtop>1000){
   				$(window).scrollTop(1000);
   				scrtop =1000;
   				time = setInterval(function(){
   				scrtop = scrtop-100;
   				$(window).scrollTop(scrtop);
   				//alert(scrtop);
   				if(scrtop <= 0){
   					clearInterval(time);   			
   				}
   				},20);

   			}
            return false;		
   });
</script>